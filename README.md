# Reconnaissance audiovisuelle de séquences de batterie #
**Homework M2RI / P4 / FAV – Nancy Bertin (*nancy.bertin@irisa.fr*), 2014**


## Contexte ##
Une très grande proportion de la musique dite "populaire" contient des **sons percussifs**, et en particulier des sons produits par la batterie. Les rythmes et séquences utilisées peuvent être notamment très caractéristiques du genre musical. La capacité à les **reconnaître et les transcrire** constitue donc un enjeu important dans le cadre de l’indexation automatique de la musique. Les sons produits par la batterie sont de nature bien différente des sons produits par la plupart des autres instruments de l’orchestre ; leur analyse et leur classification nécessite donc des traitements spécifiques, que l’on se propose d’explorer ici.

## Objectifs ##
L’objectif de ce travail personnel est de concevoir et tester différents systèmes de **reconnaissance de batterie**, et en particulier de les comparer suivant la ou les modalités qu’ils exploitent :

* la modalité audio seule,
* la modalité vidéo seule,
* ces deux modalités conjointement.

Le travail incluera l’extraction des caractéristiques (*features*) audio et vidéo, la mise en œuvre de classifieurs connus ainsi qu’une évaluation soignée et critique des résultats obtenus. L’objectif n’est pas forcément de reproduire ou concurrencer l’état de l’art, la bibliographie fournie est là comme source d’inspiration. Il s’agit de faire des choix motivés, de montrer sa capacité à prendre en main et utiliser des outils, d’obtenir un système fonctionnel, et d’avoir un regard critique sur ses performances. Pour l’extraction des caractéristiques et les classifieurs, on pourra au choix utiliser des outils disponibles (*YAAFE, Weka...*) ou programmer ses propres routines dans le langage de son choix.

## Utilisation

### Dépendances

* Python 2.7
* OpenCV 2.4.9+ (**Note**: la version 3.0+ ne convient pas)
* Yaafe
- scikit-learn

### Fixer les chemins

Il convient de définir les variables d'environnements suivant :

* DRUMS_FOLDER : dossier qui contient les sous-dossiers ENST-Drums-Audio etc
* DRUMS_OUTPUT : résultats de la reconnaissance
* DRUMS_FEATURES : doit contenir les sous-dossiers drummerI/features pour I \in {1, 2, 3}

Les chemins doivent finir par /

### Evaluation

Le script *eval.py* permet de lancer la détection pour une séquence de batterie donnée en choisissant la modalité à utiliser.
Les sorties sont placées dans le dossier *Outputs* et consistent en un fichier contenant les annotations prédites ainsi que les mesures d'erreurs correspondantes, et un fichier vidéo contenant la vidéo d'origine (angle 2) avec annotation visuelle lorsqu'un instrument est frappé d'après la prédiction.

### Modalité Audio

Extraire les caractéristiques avec le script *extract_features* dans *Code/Audio*.
Lancer le script *audio_classification* dans *Code/Audio* après avoir fixé les paramètres adéquats.

Le script sauvegarde les matrices servant à l'apprentissage sous la forme *featureI* dans le dossier courant, et les classes correspondantes, sous la forme *targetI*.
Le modèle d'apprentissage est aussi sauvegardé sous la forme *learningI*.



### Modalité Vidéo

Le script *video_features* permet de calculer les features (impact, optical flow, motion) ainsi que le score combiné pour une vidéo. Les features ainsi calculés sont sauvegardés dans le dossier *Code/Video/Features/drummer_d/angle_a*.

Le script *init_mask* crée un dictionnaire contenant des informations sur les différents instruments à partir des images segmentées se trouvant dans le dossier *Ellipses_Map*.