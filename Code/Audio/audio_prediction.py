#!/usr/bin/env python
# -*- coding: utf-8 -*-

from sklearn.externals import joblib
from audio_classification import extract_onsets
import features_functions

##Class to write

def load_model(path):
    """Load model previously trained with audio_classification"""
    return joblib.load(path)
    

def features_from(sample, num_drummer,onsets_file):
    """From an audio file, return a matrix onsets/features using annotations for the onsets"""
    onsets = extract_onsets(onsets_file)
    features = features_functions.extract_windows_all_features_one_audio_file(onsets, sample, num_drummer)#num_drummer is a string!
    return (features, onsets)
    
