#!/usr/bin/env python
# -*- coding: utf-8 -*-
from yaafelib import *
import os
import shutil

fp = FeaturePlan(sample_rate = 44100)
fp.addFeature('AttackEnergy: Energy blockSize=512 stepSize=256')
fp.addFeature('TempCentroid: EnvelopeShapeStatistics EnDecim=200  blockSize=32768  stepSize=16384')
fp.addFeature('Loundness: Loudness FFTLength=0  FFTWindow=Hanning  LMode=Relative  blockSize=1024  stepSize=512')
fp.addFeature('MFCC: MFCC CepsIgnoreFirstCoeff=1  CepsNbCoeffs=13  FFTWindow=Hanning  MelMaxFreq=6854.0  MelMinFreq=130.0  MelNbFilters=40  blockSize=1024  stepSize=512')
fp.addFeature('SF: SpectralFlatness FFTLength=0  FFTWindow=Hanning  blockSize=1024  stepSize=512')
fp.addFeature('SFPB: SpectralFlatnessPerBand FFTLength=0  FFTWindow=Hanning  blockSize=1024  stepSize=512')
fp.addFeature('SSS: SpectralShapeStatistics FFTLength=0  FFTWindow=Hanning  blockSize=1024  stepSize=512')
fp.addFeature('ZCR: ZCR blockSize=1024  stepSize=512')

df = fp.getDataFlow()

engine = Engine()
engine.load(df)

afp = AudioFileProcessor()

for i in range(3):
    audio_dir = os.getenv('DRUMS_FOLDER', '/media/Hard Disk Louboutin/') + 'ENST-Drums-Audio/drummer_'+str(i+1)+'/audio'
    features_dir = os.getenv('DRUMS_FEATURES', '/home/coco/Documents/Cours/M2/FAV/') + 'drummer_'+str(i+1)+'/features'

    for directory in os.listdir(audio_dir):
        audio = audio_dir+'/'+directory
        feats = features_dir+'/'+directory
        if not os.path.exists(feats):
            os.mkdir(feats)
        for path, dirs, files in os.walk(audio):
            for filename in files:
                afp.setOutputFormat('csv','/',{'Precision':'8'})
                afp.processFile(engine,audio+'/'+filename)
        for path, dirs, files in os.walk(audio):
            for filename in files:
                if filename.find('.csv') > 0:
                    shutil.move(audio+'/'+filename, feats+'/'+filename)
    



