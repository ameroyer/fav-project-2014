#!/usr/bin/env python
# -*- coding: utf-8 -*-
import shutil
import csv
import os

features = [['AttackEnergy', 512, 256, 20],
            ['TempCentroid', 32768, 16384, 1],
            ['Loundness', 1024, 512, 10],
            ['MFCC', 1024, 512, 10],
            ['SF', 1024, 512, 10],
            ['SFPB', 1024, 512, 10],
            ['SSS', 1024, 512, 10],
            ['ZCR', 1024, 512, 10]]

path_features = os.getenv('DRUMS_FEATURES', '/home/coco/Documents/Cours/M2/FAV/')

def extract_windows_one_feature_at_instant(t, w_size, step_size, nb_windows, name):
    #attention, on suppose que nb_windows restera le meme pour le feature.
    csvfile = open(name,"r")
    #print all_t
    cr = csv.reader(csvfile,delimiter=",")
    t_row = []
    j = 1
    end = False
    begin = True
    computed_windows = 0
    for row in cr:
        for k in range(len(row)):
            begin = (row[k].find('%') <> -1) or False
        if begin:
            j += 1
        else:
            if (not end) and (float((cr.line_num - j) * step_size + w_size)/44100 > t):
                if (computed_windows >= nb_windows):
                    end = True
                else:
                    computed_windows += 1
                    for d in row:
                        t_row.append(float(d))
    csvfile.close()
    return t_row

def extract_windows_one_feature(all_t, w_size, step_size, nb_windows, name):
    #t_list est suppose triee
    t_rows = []
    for t in all_t:
        t_row = extract_windows_one_feature_at_instant(t, w_size, step_size, nb_windows, name)
        t_rows.append(t_row)
    return t_rows
    
def extract_windows_all_features_one_audio_file(all_t, name, num_drummer):
    #name est de la forme 001_blabla , attention, on va quand meme
    #chercher les features sur tous les enregistrement du batteur Les
    #veteurs generes sont tres gros.
    path = path_features+'drummer_'+num_drummer+'/features'
    #print path
    feats = [[] for t in all_t]
    for paths, dirs, f in os.walk(path):
        for directory in dirs:
            if directory <> 'accompaniment' and directory <> 'dry_mix' and directory <> 'wet_mix' and directory <> '.DS_Store': 
                for feat in features:
                    # We ignore MFCC features 
                    if feat[0] <> 'MFCC':
                        fileName = path+'/'+directory+'/'+name+'.wav.'+feat[0]+'.csv'
                        print fileName
                        t_rows = extract_windows_one_feature(all_t, feat[1], feat[2], feat[3], fileName)
                        for i in range(len(all_t)):
                            feats[i] += t_rows[i]
    return feats


if __name__ == '__main__':
    all_t = [0.098685,1.491882,2.879274]
    rows = extract_windows_all_features_one_audio_file(all_t, "028_hits_cowbell_mallets_x4", "1")
    #rows = extract_windows_one_feature(all_t, 1024, 512, 3 ,path_features+"test4.csv")
    print rows
    print "Dimensions: ", len(rows), "x", len(rows[0])
