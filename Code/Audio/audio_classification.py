#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import features_functions
import csv
import numpy as np
import pickle


from time import time

from sklearn.cross_validation import KFold, cross_val_score, train_test_split
from sklearn import metrics
from sklearn.metrics import classification_report
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC, NuSVC
from sklearn.linear_model import LogisticRegression
from sklearn.grid_search import GridSearchCV
from sklearn.linear_model import SGDClassifier
from sklearn.externals import joblib

num_drummer = '1'

features_folders = os.getenv('DRUMS_FEATURES', '/home/pierre/info/info3/FAV/DrumsFeatures/')
drums_folder = os.getenv('DRUMS_FOLDER', '/run/media/pierre/TOSHIBA EXT/ENST_Drums/')
annotations_folder = drums_folder + "ENST-Drums-Audio/drummer_"+ num_drummer + "/annotation/"

#Les dix premiers fichiers du drummer num_drummer
drums_files_drummer = os.listdir(annotations_folder)
first_annot = drums_files_drummer[:40] # there is no file nb 8
print first_annot
first = map(lambda s : os.path.splitext(s)[0], first_annot)

def extract_onsets(annot):
    """From an annotation file, or a list of onsets, extract list of onset"""
    with open(annot, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=' ')
        onsets = [line[0] for line in reader] # onset are in the first column
        return onsets
        
        
def extract_onsets_instruments(annot):
    """extract onsets and instrument from an annotation or a result file"""
    with open(annot, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=' ')
        lines = [line for line in reader]
        return zip(*lines)
        
          
## Loading datasets
t0 = time()
print "Loading datasets for drummer " + num_drummer
features_list=[]   
target_list=[] #target can be a vector of strings!
features = np.array([])
target = np.array([])

end = num_drummer + ".pkl"
try:
    print "Trying to load with pickle"
    input_features = open("features"+end, "rb")
    features = pickle.load(input_features)
    input_features.close()
    
    print features

    input_target = open("target"+end, "rb")
    target = pickle.load(input_target)
    input_target.close()

    print "Loaded by pickle"
except:
    print "Extracting features"
    n_ignored=0
    for annot in first:
        path =  annotations_folder + annot + ".txt"
        print path
        onsets_lit, instruments = extract_onsets_instruments(path)#[float(onset) for onset in extract_onsets(path)]
        onsets = [float(onset) for onset in onsets_lit]
        try:
            features_file = features_functions.extract_windows_all_features_one_audio_file(onsets, annot, num_drummer)
        except IndexError:
            print "Ignoring file due to mismatch between number of onsets and associated features"
            n_ignored = n_ignored  + 1
            continue
        else:
            target_list.extend(instruments)
            features_list.extend(features_file)
        
    features = np.array(features_list)
    target = np.array(target_list)
    del features_list
    del target_list

    output_features = open("features"+end, "wb")
    pickle.dump(features, output_features)
    output_features.close()
    output_target = open("target"+end, "wb")
    pickle.dump(target, output_target)
    output_target.close()

    print("Ignored %d files due to mismatch" % n_ignored)
    
print("Loading done in %fs" % (time() - t0))
print "Got a matrix ", features.shape, " and dtype:", features.dtype


##Classification
print "Training"


def print_accu(name, scores):
        print("Accuracy for %s: %0.2f (+/- %0.2f)\n" % (name, scores.mean(), scores.std() * 2))
        
        
def split_test_train():
    # Split the dataset in two equal parts
    X_train, X_test, y_train, y_test = train_test_split(
        features, target, test_size=0.5)

    t0 = time()
    print "Random forests"
    randForests_clf = RandomForestClassifier(n_jobs=-1, verbose=1, n_estimators=30, max_depth=None,min_samples_split=1, oob_score=True)
    randForests_clf.fit(X_train, y_train)
    print("Training done in %f" % (time() -t0)) 

    y_true, y_pred = y_test, randForests_clf.predict(X_test)
    print(classification_report(y_true, y_pred))
    
    print "Saving model"
    
    joblib.dump(randomForests_clf, 'training' + end)
    
    print "Saving done."

    """t0 = time()
    print "K nearest neighbors"
    nn_clf = KNeighborsClassifier()
    nn_clf.fit(X_train, y_train)
    print("Training done in %f" % (time() -t0)) 

    y_true, y_pred = y_test, nn_clf.predict(X_test)
    print(classification_report(y_true, y_pred))  

    t0 = time()
    print "SVC"
    svm_clf = SVC(cache_size=500, kernel='rbf', C=0.5)
    svm_clf.fit(X_train, y_train)
    print("Training done in %f" % (time() -t0)) 

    y_true, y_pred = y_test, svm_clf.predict(X_test)
    print(classification_report(y_true, y_pred))  

    t0 = time()
    print "SGD"
    sgd_clf = SGDClassifier(n_jobs=-1)
    sgd_params = {
        'alpha': (0.00001, 0.000001),
        'penalty': ('l2', 'elasticnet'),
        'n_iter': (10, 50, 80),
    }
    sgd_grid_search = GridSearchCV(sgd_clf, sgd_params, verbose=-1, n_jobs=-1, scoring='f1')
    sgd_grid_search.fit(X_train, y_train)
    print("Training done in %f" % (time() -t0)) 

    y_true, y_pred = y_test, sgd_grid_search.predict(X_test)
    print(classification_report(y_true, y_pred))  """


def cross_validation_test():
    #Random forests
    t0 = time()
    randForests_clf = RandomForestClassifier(n_jobs=-1, verbose=1, n_estimators=30, max_depth=None,min_samples_split=1, oob_score=True)


    randForests_scores = cross_val_score(randForests_clf, features, target, scoring='f1')

        
    print_accu("Random forests", randForests_scores)

    print("Training done in %f" % (time() -t0)) 


    """#Nearest neighbours (should be bad)
    t0 = time()
    nn_clf = KNeighborsClassifier()

    nn_scores = cross_val_score(nn_clf, features, target, scoring='f1')

        
    print_accu("K Nearest neighbours", nn_scores)

    print("Training done in %f" % (time() -t0)) 

    # SVM
    t0 = time()
    svm_clf = SVC()

    svm_scores =  cross_val_score(svm_clf, features, target,  scoring='f1')

    print_accu("SVM", svm_scores)

    print("Training done in %f" % (time() -t0)) 

    # Logistic regression
    t0 = time()
    lr_clf = LogisticRegression()

    lr_scores =  cross_val_score(lr_clf, features, target)

    print_accu("Logistic regression", lr_scores)

    print("Training done in %f" % (time() -t0)) 
    

    # Stochastic gradient descent - SGD
    t0 = time()
    sgd_clf = SGDClassifier(n_jobs=-1)
    sgd_params = {
        'alpha': (0.00001, 0.000001),
        'penalty': ('l2', 'elasticnet'),
        'n_iter': (10, 50, 80),
    }
    sgd_grid_search = GridSearchCV(sgd_clf, sgd_params, verbose=-1, n_jobs=-1, scoring='f1')

    sgd_scores = cross_val_score(sgd_grid_search, features, target,  scoring='f1')    

    print_accu("SGD", svm_scores)

    print("Training done in %f" % (time() -t0)) """
 
split_test_train() 

cross_validation_test()  
    
