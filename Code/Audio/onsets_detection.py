#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import numpy as np
import csv

drums_folder=os.getenv("DRUMS_FOLDER", "/run/media/pierre/TOSHIBA EXT/ENST_Drums/")
output_folder=os.getenv("DRUMS_OUTPUT", "/run/media/pierre/TOSHIBA EXT/Outputs/")


def extract_onsets(annot):
    """From an annotation file, or a list of onsets, extract list of onset"""
    with open(annot, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=' ')
        onsets = [line[0] for line in reader] # onset are in the first column
        print onsets
        return onsets


mse = 0.

for num_drummer in range(1,4):
    audio_folder= drums_folder + "/ENST-Drums-Audio/drummer_" + str(num_drummer) + "/audio/"
    annotations_folder = drums_folder + "/ENST-Drums-Audio/drummer_" + str(num_drummer) + "/annotation/"
    # Generate onsets
    for directory in os.listdir(audio_folder):
        if directory <> 'accompaniment' and directory <> 'dry_mix' and directory <> 'wet_mix' and directory <> '.DS_Store': 
            d = audio_folder+directory+"/"
            for f in os.listdir(d):
                basename = os.path.splitext(f)[0]
                command = "aubioonset -i \"" + d + f + "\" -O hfc > \"" + output_folder + basename + "_onsets_aubio_drummer_"+ str(num_drummer) +".csv\""
                print command
                os.system(command)
    
    # Comparing with annotations
    # Comparison is done in eval.py
    """for f in os.listdir(annotations_folder):
        basename = os.path.splitext(f)[0]
        onsets_target = extract_onsets(annotations_folder + f)
        onsets_predicted = extract_onsets(output_folder + basename + "_onsets_aubio_drummer_"+ str(num_drummer) +".csv")
        onsets_t = np.array([float(onset) for onset in onsets_target])
        onsets_p = np.array([float(onset) for onset in onsets_predicted])
        #mse =  mse + ((onsets_t - onsets_p) ** 2.)"""
        
#Mean squared error       
#print ("MSE : %f" % (mse.mean(axis=None)))
        
