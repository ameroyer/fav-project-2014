#!/usr/bin/env python
# -*- coding: utf-8 -*-

import cv2
import numpy as np
from init_mask import init_ellipses
from multiprocessing import Process, cpu_count, Queue
from display_score import display

"""
Script gérant le calcul des features en parallèle
"""


def compute_feature_frame(frame, ante_frame, gmm, ellipses, motion_threshold = 0.9):
    """
    Calcul des features pour une frame
    frame
    ante_frame: frame précédente
    gmm : modèle de l'arrière plan (mélange de gaussiennes)
    ellipses : dictionnaire contenant les ellipses
    motion_threshold: seuil pour le feature de mouvement des instrument
    """
    
    #Optical Flow
    prvs = cv2.cvtColor(ante_frame, cv2.COLOR_BGR2GRAY)
    next = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    flow = cv2.calcOpticalFlowFarneback(prvs, next, 0.5, 3, 50, 3, 15, 2.0, 0)  
    mag, ang = cv2.cartToPolar(flow[:,:,0], flow[:,:,1], angleInDegrees = True)

    #Impact Feature
    fgmask = gmm.apply(frame, learningRate=0.001)
    
    #Motion feature
    sobel= cv2.Sobel(next,cv2.CV_64F,1,1,ksize=15)

    
    #Browse Ellipses    
    impact_score = [0] * len(ellipses)
    flow_score = [0] * len(ellipses)
    sobel_score = [0] * len(ellipses)

    
    for i, ri in enumerate(ellipses.values()):
        for x,y in ri['pixels']:
            if fgmask[x,y] != 0:
                flow_score[i] += mag[x,y] * np.exp(- min(abs(ang[x,y] - 270), abs(ang[x,y] + 90)))
                impact_score[i] += fgmask[x,y]
            elif abs(sobel[x,y]) > motion_threshold * 255:
                sobel_score[i] += abs(sobel[x,y])
            
    max_score = np.sum(flow_score)
    if max_score != 0:
        flow_score = [ float(c) / max_score for c in flow_score]
    max_score = np.sum(impact_score)
    if max_score != 0:
        impact_score = [ float(c) / max_score for c in impact_score]
    max_score = np.sum(sobel_score)
    if max_score != 0:
        sobel_score = [ float(c) / max_score for c in sobel_score]


    ## #Combinations
    ## score = [0] * len(ellipses)
    ## for i in xrange(len(ellipses)):
    ##     score[i] =  impact_score[i] * flow_score[i] * sobel_score[i]
    return impact_score, flow_score, sobel_score



def normalize_score(features, lookup = 125):
    """
    Given a feature array, normalize the scores (noisy values) and brings it between 0 and 1
    """
    final_scores = np.zeros_like(features)
    ellipses, frames = features.shape
    for n in xrange(frames):
        left = max(0, n-lookup)
        right = min(n+lookup, frames)
        trim = features[:, left:right]
        size = right - left 
        #Median insteand of empirical mean (noise)
        mu = np.median(trim, axis = 1) 
        #Trimmed Variance instead of variance (noise)
        sigma = np.zeros(ellipses)
        for i in xrange(ellipses):
            t = np.sort(trim[i,:])
            t = t[:0.9*size]
            t = t[0.1*size:]            
            sigma[i] = np.std(t)
        sigma[sigma == 0] = 0.0001
        #Final Impact Features
        final_scores[:,n] =  1.0 - 1.0/(np.sqrt(2*3.14)) * np.exp(- 0.5* ((features[:,n] - mu) /  sigma)**2)
    return final_scores





def prediction(video_path, ellipses):
    """
    Compute features for a video (non-threaded version)
    """
    cap = cv2.VideoCapture(video_path)
    frames = int( cap.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT))
    features = np.zeros((len(ellipses), frames))
    
    fgbg = cv2.BackgroundSubtractorMOG()
    _, prvs = cap.read()
    count = 0 
    while(count < frames):
        ret, frame = cap.read()
        im, fl, mo = compute_feature_frame(frame, prvs, fgbg, ellipses)
        features[:, count] = s
        prvs = frame
        count += 1
        print "Impact Feature: %i/%i"%(count, frames)

    cap.release()
    cv2.destroyAllWindows()

    im = normalize_score(im)
    fl = normalize_score(fl)
    mo = normalize_score(mo)
    
    final_scores = im * fl * mo           
        
    return frames, final_scores





########------------------ Version parallelisée
def process_prediction(thread_id, indices, frames, fgbg, ellipses, queue):
    """
    Function executed by one thread
    """
    print "Thread %d - start." %thread_id
    for i, (index, f) in enumerate(zip(indices[1:], frames[1:])):
        print "%d  - %d/%d"%(thread_id, i+1, len(frames))
        s = compute_feature_frame(f, frames[i-1], fgbg, ellipses)
        queue.put((index, s))
        k = cv2.waitKey(30) & 0xff
        if k == 27:
            break
    queue.close()
    queue.join_thread()
    print "Thread %d - done." %thread_id
        
    



def parallel_prediction(video_path, ellipses, cores = 3):
    """
    Compute features for a video (threaded version)
    """
    #Setting number of cores
    cpuc = cpu_count()
    if cores < 1:
        print "Warning: Core Number too low. Setting it to 1"
        cores = 1
    elif cores > cpuc:
        print "Warning: Core Number exceeds cpu cores. Setting it to %d" %cpuc
        cores = cpuc


    ##Init
    print "Reading Video"   
    cap = cv2.VideoCapture(video_path)
    frames = int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT))
    features = np.zeros((len(ellipses), frames))
    
    im = np.zeros((len(ellipses), frames))
    fl = np.zeros((len(ellipses), frames))
    mo = np.zeros((len(ellipses), frames))
    
    count = 0
    frame_pics = [0] * frames    
    while(count < frames):
        _, frame_pics[count] = cap.read()
        count += 1

    
    #---- Compute feature for all frames
    print "Parallel Computation of features"
    fgbg = cv2.BackgroundSubtractorMOG()
    processi = []
    resqueue = Queue()

    start = 0
    to_send = 0
    for i in xrange(cores):
        indices = range(start, start + frames / 3)
        f = [frame_pics[x] for x in indices]
        to_send += len(f) - 1
        t = Process(target = process_prediction, args=(i, indices, f, fgbg, ellipses, resqueue,))
        processi.append(t)
        start += frames / 3 - 1
        t.start()

    #--- Get result from queue
    for n in xrange(to_send):
        (i, obj) = resqueue.get()
        impact, flow, motion = obj
        im[:, i] = impact
        fl[:, i] = flow
        mo[:, i] = motion

    for i, t in enumerate(processi):
        t.join()
        print 'joined thread %d'%i

    #---- Return
    
    im = normalize_score(im)
    fl = normalize_score(fl)
    mo = normalize_score(mo)
    
    final_scores = im * fl * mo
    
    return frames, final_scores, im, fl, mo

        





if __name__ == '__main__':
    #####PATHES
    code_folder = "/home/amelie/Projets/FAV/Code/Video/"
    video_folder = "/media/windows/Documents and Settings/Amelie/Desktop/ENST Drums/ENST-Drums-Video/"


    ######################## 1 VIDEO
    
    #Parameters
    angle = 2
    drummer = 1
    
    #Load Ellipses Dictionnary
    video_path = video_folder + "drummer_%d/video/angle_%d/001_hits_snare-drum_sticks_x6.mp4"%(drummer, angle)
    mask_path = code_folder + "Ellipses_Map/drummer_%d/angle_%d_segmented.png"%(drummer, angle)
    colormap = code_folder + "Ellipses_Map/drummer_%d/colormap.txt"%(drummer)
    ellipses = init_ellipses(mask_path, colormap)

    
    #frames, final_scores = prediction(video_path, ellipses)
    frames, final_scores, _, _, _ = parallel_prediction(video_path, ellipses)
    
    #Affichage
    print "Displaying feature results"
    display(ellipses, video_path, final_scores, "output_d%d_a%d.avi"%(drummer, angle), False)

    

    ################################# BATCH
    #Loop pour la Sauvegarde des features (l par l, pour chaque drummer)
    raise SystemExit #Comment/Uncomment if batch or not
    t = 0 #current start
    l = 10 #period
    cont = True
    while cont:
        cont = False

        #Loop on Drummer
        for drummer in [1,2,3]:
            colormap = code_folder + "Ellipses_Map/drummer_%d/colormap.txt"%(drummer)

            #Loop on Angle
            for angle in [1,2]:
                output_folder = "Features/drummer_%d/angle_%d/" %(drummer, angle)
                print output_folder
                if not os.path.exists(output_folder):
                    os.makedirs(output_folder)
                mask_path = code_folder + "Ellipses_Map/drummer_%d/angle_%d_segmented.png"%(drummer, angle)

                #Set params
                ellipses = init_ellipses(mask_path, colormap)
                header = ' '.join([','.join(x['label']) for x in ellipses.values()])

                folder = video_folder + "drummer_%d/video/angle_%d/"%(drummer, angle)

                #Compute features for the current batch
                videos = [f for f in os.listdir(folder) if f.endswith('.mp4')]
                if len(videos) >= t+l:
                    cont = True
                for v in videos[t:t+l]:
                    print  v
                    video_path = folder + v
                    _, _, impact_scores, flow_scores, motion_scores = parallel_prediction(video_path, ellipses)

                    #Write results
                    result_txt = ""
                    for i, (ci, cf, cm) in enumerate(zip(impact_scores.T, flow_scores.T, motion_scores.T)):
                        result_txt += " ".join(["(%.8f,%.8f,%.8f)"%(vi, vf, vm) for (vi, vf, vm) in zip(ci, cf, cm)])
                        result_txt += "\n"
                    output_file = output_folder + v.rsplit('.', 1)[0] + '.txt'
                    with open(output_file, 'w') as f:
                        f.write(header + "\n" + txt)
        t += l

    
        
    


