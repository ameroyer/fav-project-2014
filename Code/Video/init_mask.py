#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pickle
import os

"""
Script gérant la lecture d'une ellipse map et créant le dictionnaire correspondant
"""


def init_color_code(color_path):
    """
    Mapping Color to annotation
    """
    with open(color_path, 'r') as f:
        color_code = {}
        for line in f:
            args = line.split()
            r = int(args[0])
            g = int(args[1])
            b = int(args[2])
            label = args[3].split(',')
            color_code[(r,g,b)] = label
    return color_code



def init_mask(mask_path, color_code):
    """
    Build the ellipses dictionnary
    """
    from scipy import misc
    mask = misc.imread(mask_path)
    xmax, ymax, _ = mask.shape

    #Init
    ellipses = {}
    for c, label in color_code.iteritems():
        ellipses[c] = {}
        ellipses[c]['pixels'] = [] #pixels de l'ellipses
        ellipses[c]['color'] = c #couleur de l'ellipse sur l'image
        ellipses[c]['min'] = (xmax, ymax) #Bounding box point en bas à gauche
        ellipses[c]['max'] = (0, 0) #Bounding box point en haut à droite
        #Note: axe x et y inversés par la lecture de l'image
        ellipses[c]['label'] = label

    #Browse
    for x in xrange(xmax):
        for y in xrange(ymax):
            r = mask[x][y][0]
            g = mask[x][y][1]
            b = mask[x][y][2]
            label = (r,g,b)
            if label in ellipses:
                aux = ellipses[label]
                aux['pixels'].append((x,y))
                
                #Bouding Box Inf
                a, b = aux['min']
                if x < a:
                    a = x
                if y < b:
                    b = y
                aux['min'] = (a,b)
                
                #Bouding Box Sup
                a, b = aux['max']
                if x > a:
                    a = x
                if y > b:
                    b = y
                aux['max'] = (a,b)

    #Gestion des instruments n'apparaissant pas dans l'angle de vue courant
    todel = []
    for k, e in ellipses.iteritems():
        if len(e['pixels']) == 0:
            todel.append(k)
            
    for i in todel:
        del ellipses[i]
                                
    return ellipses
                
    


def init_ellipses(mask_path, colormap, output = False):
    """
    Init ellipses (if it doesnt' already exist) from a path to a mask and path to a colormap
    """
    pkl = mask_path.rsplit('.', 1)[0] + '.pkl'
    if os.path.isfile(pkl):
        #print "Loading from pickle file"
        with open(pkl, 'rb') as f:
            regions = pickle.load(f)
    else:
        color_code = init_color_code(colormap)
        regions = init_mask(mask_path, color_code)
        with open(pkl, 'wb') as f:
            print "Saving ellipses in pickle file"
            pickle.dump(regions, f)

    if output:
        print "----> Summary"
        print "%d ellipses found" %len(regions)
        count = 1
        for key, e in regions.iteritems():
            print "\nEllipse n°%d" %count
            print "  Label", ','.join(e['label'])
            print "  Color (%d, %d, %d)" %(e['color'][0], e['color'][1], e['color'][2])
            print "  Bounding box (%d,%d) - (%d, %d)" %(e['min'][0], e['min'][1], e['max'][0], e['max'][1])
            print "  %d Pixels" %len(e['pixels'])
            count += 1

    return regions



if __name__ == "__main__":
    angle = 1
    drummer = 2
    code_folder = "/home/amelie/Projets/FAV/Code/Video/"
    mask_path = code_folder + "Ellipses_Map/drummer_%d/angle_%d_segmented.png"%(drummer, angle)
    colormap = code_folder + "Ellipses_Map/drummer_%d/colormap.txt"%(drummer)
    init_ellipses(mask_path, colormap, output = True)
