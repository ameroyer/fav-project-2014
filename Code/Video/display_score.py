#!/usr/bin/env python
# -*- coding: utf-8 -*-

import cv2
import numpy as np
from init_mask import init_ellipses
from multiprocessing import Process, cpu_count, Queue
import os


"""
Script for displaying/writing a video with the features result on it
"""


def display(ellipses, video_path, final_scores, output, show = False):
    """
    Write the scores values in final_score on the videos
    """
    #For text display purposes only: precompute Mean point in ellipses
    mean_point_x = {}
    mean_point_y = {}
    for c, ri in ellipses.iteritems():
        mean_point_x[c] = (ri['min'][0] + ri['max'][0]) / 2
        mean_point_y[c] = (ri['min'][1] + ri['max'][1]) / 2
        
    # Write features values on video
    fourcc = cv2.cv.CV_FOURCC(*'FMP4')
    video_writer = cv2.VideoWriter(output, fourcc, 24.0, (720,576))
    font = cv2.FONT_HERSHEY_SIMPLEX
    
    cap = cv2.VideoCapture(video_path)
    frames = int( cap.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT))
    
    count = 0
    
    while(count < frames):
        ret, frame = cap.read()
        for i, (c, ri) in enumerate(ellipses.iteritems()):
            s = "%.3f" %final_scores[i, count]
            cv2.putText(frame, s, (int(mean_point_y[c] - 20), int(mean_point_x[c] +10)), font, 1, ri['color'][::-1])
        count += 1
        video_writer.write(frame)
        if show:
            cv2.imshow('frame', frame)
            k = cv2.waitKey(30) & 0xff
            if k == 27:
                break

    
    cap.release()
    video_writer.release()
    cv2.destroyAllWindows()



def display_beats(ellipses, video_path, annotation, output, show = False):
    """
    Display the beats as given by a list of pairs (time, instrument)
    """
    mean_point_x = {}
    mean_point_y = {}
    for c, ri in ellipses.iteritems():
        mean_point_x[c] = (ri['min'][0] + ri['max'][0]) / 2
        mean_point_y[c] = (ri['min'][1] + ri['max'][1]) / 2
        
    # Write features values on video
    fourcc = cv2.cv.CV_FOURCC(*'FMP4')
    video_writer = cv2.VideoWriter(output, fourcc, 24.0, (720,576))
    font = cv2.FONT_HERSHEY_SIMPLEX
    
    cap = cv2.VideoCapture(video_path)
    frames = int( cap.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT))
    
    count = 0
    
    while(count < frames):
        ret, frame = cap.read()
        try:
            inst = annotation[count * 1.0 / 24]
            cand = [x for x in ellipses.values() if inst in x['label']][0]
            s = "Hit " + inst
            cv2.putText(frame, s, (int(mean_point_y[c] - 20), int(mean_point_x[c] +10)), font, 1, cand['color'][::-1])
        except (KeyError, IndexError):
            pass
        count += 1
        video_writer.write(frame)
        if show:
            cv2.imshow('frame', frame)
            k = cv2.waitKey(30) & 0xff
            if k == 27:
                break

    
    cap.release()
    video_writer.release()
    cv2.destroyAllWindows()
