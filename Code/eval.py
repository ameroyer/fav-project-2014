#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys
from scipy import signal
import numpy as np
sys.path.append('./Video')
from impact_feature import impact_prediction
from video_features import prediction, parallel_prediction
from init_mask import init_ellipses
from display_score import display_beats
sys.path.append('./Audio')
from sklearn.ensemble import RandomForestClassifier
import audio_prediction
from fusion import combine


"""
Script Principal pour l'évaluation de la reconnaissance de drum beats
"""



def parse_video_feature(path):
    """
    Parse a file containing video features and return a score combining the features
    """
    with open(path, 'r') as f:
        lines = f.read().split("\n")
        frames = len(lines) - 1
        inst = len(lines[0].split())
        features = np.zeros((inst, frames))
        for x, l in enumerate(lines[1:]):
            for y, f in enumerate(l.split()):
                i, f, m = [float(z) for z in f.replace('(', '').replace(')','').split(',')]
                score = i * f * m
                features[y, x] = score
        return features



            
def parse_annotation(annotation_file):
    """
    Parse an annotation file and returns it in the form of a list
    """
    annotation = {}
    with open(annotation_file, 'r') as f:
        for line in f:
            t, l = line.split()
            annotation[float(t)] = l
    return annotation


##Global variables: tuples of similar instruments in order to define a refined error
conf6 = ['sd', 'sd-']

conf1 = ['chh', 'ohh']
conf2 = ['lt', 'ltr']
conf3 = ['mt', 'mtr']

conf4 = ['c', 'rc', 'ch', 'cr', 'chh', 'ohh']
conf5 = ['lmt', 'lt', 'ltr', 'mt', 'mtr', 'lft']
def instrument_error(inst1, inst2):
    global conf1, conf2, conf3, conf4, conf5
    """
    Compute an error if instrument 1 is predicted instead of instrument 2
    """
    if inst1 == inst2:
        return 0.0
    elif (inst1 in conf6 and inst2 in conf6):
        return 0.1
    elif (inst1 in conf1 and inst2 in conf1) or (inst1 in conf2 and inst2 in conf2) or (inst1 in conf3 and inst2 in conf3):
        return 0.5
    elif (inst1 in conf4 and inst2 in conf4) or (inst1 in conf5 and inst2 in conf5):
        return 0.7
    else:
        return 1.0



    
    
def compare_annotations(annotation, ground_truth):
    """
    Compute errors by comparing annotation and ground truth
    """
    time_error = 0.0   #erreur sur les instants de predictions
    sound_error = 0.0  #erreur sur les instruments prédits

    max_time = max(ground_truth.keys())

    pred = {}
    duplicates = [] #Prediction double
    
    if len(annotation) == 0:
        return 1.0, 1.0, 0.0, 0.0
    
    for (time, instrument) in annotation:
        #Find closest in prediction
        closest = min(ground_truth.keys(), key = lambda x: abs(x - time))
        te = float(abs(closest - time)) / max_time
        se = instrument_error(instrument, ground_truth[closest])
        try:
            current_pred, _, terr, serr = pred[closest]
            if te + se < terr + serr:
                duplicates.append(current_pred)
                pred[closest] = (time, instrument, te, se)
        except KeyError:
            pred[closest] = (time, instrument, te, se)

    #Compute errors
    for g, i in ground_truth.iteritems():
        try:
            _, _, te, se = pred[g]
            time_error += te
            sound_error += se
        except KeyError:
            continue
    precision = float(len(pred)) / float(len(annotation))
    rappel = float(len(pred)) / float(len(ground_truth))
            
    return time_error / len(annotation), sound_error / len(annotation), precision, rappel





####------------------------------------------------------- MAIN           

def eval_one_sample(drummer, number, enst_folder, video_threshold = 0.8, audio_clf, mode = 0):
    """
    Renvoie l'erreur pour le fichier "number" du drummer "drummer"

    mode: 0 = video only
          1 = audio only
          2 = video + audio
    """

    #Load ground truth and file name
    annotation_folder = enst_folder + "ENST-Drums-Audio/drummer_%d/annotation/"%drummer
    annotation_file = [f for f in os.listdir(annotation_folder) if f.endswith('.txt') and f.startswith("%03d" %number)]
    try:
        annotation_file = annotation_file[0]
    except IndexError:
        print "Number %d not found for Drummer %d"%(number, drummer)
        raise SystemExit
    file_name = annotation_file.rsplit('.', 1)[0]
    print file_name

    
    
    #------------------ Classifier results

    #### -------------------- Video
    video_result = []
    colormap = "Video/Ellipses_Map/drummer_%d/colormap.txt"%(drummer)

    #Define union of labels for case when both view angles display different instruments
    labels = {}
    indlabels = {}
    with open(colormap, 'r') as f:
        count = 0
        for l in f:
            labels[l.split()[3].split(',')[0]] = count
            indlabels[count] = l.split()[3].split(',')[0]
            count +=1
            
    video_scores = {}
    #Pour les deux angles
    for angle in [1, 2]:
        mask_path = "Video/Ellipses_Map/drummer_%d/angle_%d_segmented.png"%(drummer, angle)
        ellipses = init_ellipses(mask_path, colormap)

        #Compute Feature (or directly load them if precomputed)
        feature = "Video/Features/drummer_%d/angle_%d/%s.txt"%(drummer, angle, file_name)
        if os.path.isfile(feature):
            aux = parse_video_feature(feature)
        else:
            video = enst_folder + "ENST-Drums-Video/drummer_%d/video/angle_%d/%s.mp4"%(drummer, angle, file_name)
            _, aux, _, _, _ = parallel_prediction(video, ellipses)

        #Combine both angles
        for k, rk in enumerate(ellipses.values()):
            index = labels[rk['label'][0]]
            try:
                video_scores[index] = (video_scores[index] * aux[k, :])
            except KeyError:
                video_scores[index] = aux[k, :]
            
    

    #Peak detections + Thresholding
    if mode == 0:
        width_param = 4 #width param for peak detection
        step = 1.0 / 24
        for i in xrange(count):
            if not i in video_scores:
                print indlabels[i]
                continue
            peak_ind = signal.find_peaks_cwt(video_scores[i], np.arange(1,width_param))
            for x in peak_ind:
                if video_scores[i][x] > video_threshold:
                    video_result.append((x * step,  indlabels[i])) #todo
                


    #### -------------------- Audio
    # We use here the onsets from the annotation files
    # but we could the onsets calculated with another method (see Code/Audio/onsets_detection.py which uses
    # Aubioonset with the high frequency content method)
    (features,onsets) = audio_prediction.features_from(annotation_file, drummer, annotation_file)
    clf = audio_prediction.load_model("training"+drummer+".pkl")#the file is probably located in Code/Audio
    instruments_results = clf.predict(features)
    audio_result = zip(onsets, instruments)
    

    #------------------- Combination
    
    # TODO: it is likely to be better to compute the previous results after choosing which one we want
    if mode == 0:
        result = video_result
    elif mode == 1:
        result = audio_result
    elif mode == 2:
        result = combine(video_scores, audio_scores) 


    
    #------------------- Evaluation
    ground_truth = parse_annotation(annotation_folder + annotation_file)
    time_error, sound_error, pre, rap = compare_annotations(result, ground_truth)

    return file_name, result, time_error, sound_error, pre, rap



if __name__ == "__main__":
    
    drummer = 1
    number = 3
    
    enst_folder = os.getenv('DRUMS_FOLDER', "/media/windows/Documents and Settings/Amelie/Desktop/ENST Drums/")
    output_folder = os.getenv('DRUMS_OUTPUT', 'Outputs/')
        

    fil, result, te, se, pr, rap = eval_one_sample(drummer, number, enst_folder)

    #Write Results
    with open('Outputs/output_%s.txt'%fil, 'w') as f:
        st = "Temporal error: %.5f \n Instrument error: %.5f \n Precision: %.5f \n Recall: %.5f \n\n Annotation \n"%(te, se, pr, rap) + "\n".join([ "%.3f %s"%(t,l) for (t,l) in result])
        f.write(st)
    print "Output in", 'Outputs/output_%s.txt'%fil

    
    #Write Video (visual results)
    angle = 2
    mask_path = "Video/Ellipses_Map/drummer_%d/angle_%d_segmented.png"%(drummer, angle)
    colormap = "Video/Ellipses_Map/drummer_%d/colormap.txt"%(drummer)
    ellipses = init_ellipses(mask_path, colormap)
    video_path = enst_folder + "ENST-Drums-Video/drummer_%d/video/angle_%d/%s.mp4"%(drummer, angle, fil)
    display_beats(ellipses, video_path, dict(result),'Outputs/video_output_%s.avi'%fil, show = False)




    ## ################ EXPERIENCES VIDEO EN COURS --- DO NOT TOUCH
    ## ###Evaluation des 3 features
    ## mean_t = 0.0
    ## mean_s = 0.0
    ## mean_p = 0.0
    ## mean_r = 0.0
    ## ## drummer = 1
    ## ## for number in xrange(1,11):
    ## ##     enst_folder = "/media/windows/Documents and Settings/Amelie/Desktop/ENST Drums/"

    ## ##     fil, result, te, se, de, re = eval_one_sample(drummer, number, enst_folder)
    ## ##     mean_t += te
    ## ##     mean_s += se
    ## ##     mean_p += de
    ## ##     mean_r += re

    ## ##     with open('Outputs/output_%s.txt'%fil, 'w') as f:
    ## ##         st = "Temporal error: %.5f \n Instrument error: %.5f \n Precision: %.5f  \n Rappel: %.5f \n\n Annotation \n"%(te, se, de, re) + "\n".join([ "%.3f %s"%(t,l) for (t,l) in result])
    ## ##         f.write(st)
    ## ##     print "Output in", 'Outputs/output_%s.txt'%fil

    ## ## with open('Outputs/output_mean.log', 'w') as f:
    ## ##     st = 'Mean temporal error %.5f \n'%(mean_t / 10.0)
    ## ##     st += 'Mean instrument error %.5f \n'%(mean_s / 10.0)
    ## ##     st += 'Mean Precision %.5f \n'%(mean_p / 10.0)
    ## ##     st += 'Mean Recall %.5f \n'%(mean_r / 10.0)
    ## ##     f.write(st)


    ## ##Evaluation des 3 drummers
    ## number1 = range(1,30) + range(36,54) + [60,61,62]
    ## number2 = range(1,32) + range(38,56) + [69]
    ## number3 = range(1,39) + range(40,49) + [55,56,57]
    ## numbers = [number1, number2, number3]
    ## for drummer in [1, 2, 3]:
    ##     g = numbers[drummer - 1]
    ##     mean_t = 0.0
    ##     mean_s = 0.0
    ##     mean_p = 0.0
    ##     mean_r = 0.0
        
    ##     for number in g:
    ##         enst_folder = "/media/windows/Documents and Settings/Amelie/Desktop/ENST Drums/"
    ##         fil, result, te, se, de, re = eval_one_sample(drummer, number, enst_folder)
    ##         mean_t += te
    ##         mean_s += se
    ##         mean_p += de
    ##         mean_r += re

    ##         with open('Outputs/drummer_%d/output_%s.txt'%(drummer,fil), 'w') as f:
    ##             st = "Temporal error: %.5f \n Instrument error: %.5f \n Precision: %.5f  \n Rappel: %.5f \n\n Annotation \n"%(te, se, de, re) + "\n".join([ "%.3f %s"%(t,l) for (t,l) in result])
    ##             f.write(st)

    ##     with open('Outputs/drummer_%d/output_mean.log'%drummer, 'w') as f:
    ##         st = 'Mean temporal error %.5f \n'%(mean_t / len(g))
    ##         st += 'Mean instrument error %.5f \n'%(mean_s / len(g))
    ##         st += 'Mean Precision %.5f \n'%(mean_p / len(g))
    ##         st += 'Mean Recall %.5f \n'%(mean_r / len(g))
    ##         f.write(st)
            
    
    
    
